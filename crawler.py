from datetime import datetime
from time import sleep
import requests
from satl import Satl
from bs4 import BeautifulSoup
import re
import os

from utils.printer import printer

root_dir = os.path.dirname(os.path.abspath(__file__))
base_url = 'https://www.tripadvisor.com'
top_activity_url = base_url + '/Attractions-%s-%sActivities-%s.html'
top_restaurants_url = base_url + '/Restaurants-%s-%sActivities-%s.html'

def get_page(url):
    if ('Attraction_Review') in url:
        headers = {
            #'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0'
            #'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36'
            #'User-Agent': 'Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19'
            'User-Agent': 'Mozilla/5.0 (Linux; Android 7.0; Pixel C Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/52.0.2743.98 Safari/537.36'
        }
        printer('blue', 'Get', url)
    
        try:
            html = requests.get(url, headers=headers)
        except:
                pass
        sleep(10)
        soup = BeautifulSoup(html.content, 'html.parser')
        print(soup)
        return soup
    
    else:
        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:55.0) Gecko/20100101 Firefox/55.0'
            #'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36'
            #'User-Agent': 'Mozilla/5.0 (Linux; Android 4.0.4; Galaxy Nexus Build/IMM76B) AppleWebKit/535.19 (KHTML, like Gecko) Chrome/18.0.1025.133 Mobile Safari/535.19'
            #'User-Agent': 'Mozilla/5.0 (Linux; Android 7.0; Pixel C Build/NRD90M; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/52.0.2743.98 Safari/537.36'
        }
        printer('blue', 'Get', url)

        try:
            html = requests.get(url, headers=headers)
        except:
                pass
        sleep(1)
        soup = BeautifulSoup(html.content, 'html.parser')
        return soup


#    from selenium import webdriver
#    import selenium as se
#    from selenium.webdriver.support import expected_conditions as EC
#    from selenium.webdriver.common.by import By
#    from selenium.common.exceptions import TimeoutException
#    from selenium.webdriver.support.ui import WebDriverWait

    
    #options = se.webdriver.ChromeOptions()
    #options.add_argument('headless')

#    driver = webdriver.Chrome(root_dir + '/chromedriver')
#
#    html = driver.get(url)
#
#    htmlContent = driver.page_source
#    #sleep(5)
#    import selenium.webdriver.support.ui as ui
#    if ('Attraction_Review') in url:
#        driverr = webdriver.Chrome(root_dir + '/chromedriver')
#        driverr.get(url)
#        timeout = 5
#        try:
#            element_present = EC.presence_of_element_located((By.XPATH, "//div[@class='prw_rup prw_common_responsive_static_map_image staticMap']//img[@class='mapImg']"))
#            WebDriverWait(driverr, timeout).until(element_present)
#            soup = BeautifulSoup(driverr.page_source, 'html.parser')
#            print(soup)
#            return soup
#        except TimeoutException:
#            print "Timed out waiting for page to load"


        #print(htmlContent)
        #wait_for_condition("""for (var i = 0; i<window.maxs_markers.length;i++) {
        #   google.maps.event.trigger(window.maxs_markers[i],'click')
        #   }""",driver)
        #       #driver.execute_script("""for (var i = 0; i<window.maxs_markers.length;i++) {
        #       #google.maps.event.trigger(window.maxs_markers[i],'click')
        #       #}""")
        #markers = driver.execute_script('return window.maxs_markers_infowindow')
        #print driver.execute_script('return window.maxs_markers_latlng')

    #driver.implicitly_wait(20)
    #wait_for_condition('!DemandLoadAjax.isLoading()',driver)
#        wait = ui.WebDriverWait(driver, 15)
#        #wait.until(EC.visibility_of_element_located((By.PARTIAL_LINK_TEXT, "maps.google.")))
#        #mapImg = driver.find_element_by_xpath("//div[@class='prw_rup prw_common_responsive_static_map_image staticMap']//img[@class='mapImg']")
#        #print(mapImg)
#        wait.until(lambda driver: driver.find_element_by_class_name('mapImg'))
#        soup = BeautifulSoup(htmlContent, 'html.parser')
#    #    a = soup.find_all('img', 'mapImg')
#    #print(soup)
#        return soup
#    else:
#        soup = BeautifulSoup(htmlContent, 'html.parser')
#        return soup

    #sleep(15)



def wait_for_condition(c,browser):
    for x in range(1,10):
        print "Waiting for ajax: " + c
        x = browser.execute_script("return " + c)
        if(x):
            return
            time.sleep(1)


def get_text(item):
    if item:
        return item.get_text()
    return ''


def get_poi(url, data_dict, crawler_list, kind):
    page = get_page(url)
    #print(page)
    popularity = get_text(page.find('span', class_='header_popularity'))

    location = page.find(string=re.compile(r"lat: (.*?)"))
    #images_obj = page.find('div', class_='page_images').find_all('img', class_='centeredImg noscript')
    images_obj = page.find('div', class_='mini_photos_container').find_all('img', class_='basicImg')
    #print(images_obj)
    images = []
    for image in images_obj:
        images.append(image.get('src'))
    
    #google_map_url = page.find('div', class_='ppr_rup ppr_priv_location_detail_contact_card').find('div', class_='contactInfo')#.find('div', class_='section map-widget clickable').find('div', class_='prw_rup prw_common_responsive_static_map_image staticMap')#.find_all('img', class_='basicImg')
    #google_map_url = page.find('div', id='btf_wrap')#.find('div', id='taplc_location_detail_overview_resp_ar_responsive_0')#.find_all('div', class_='ui_section no-bg block_wrap')

    #google_map_url = page.find_all('div', id='taplc_location_detail_contact_card_ar_responsive_0')
    #print(google_map_url)



    #data-lat="13.761519432067871" data-lng="100.49620056152344

    #lat_pattern = re.compile(r"data-lat (.*?),", re.MULTILINE | re.DOTALL)
    #long_pattern = re.compile(r"data-lng (.*?),", re.MULTILINE | re.DOTALL)
    #lat = lat_pattern.search(google_map_url)#.group(1)
    #lng = long_pattern.search(google_map_url).group(1)

    #print(lat)
    #print(lng)

    description = get_text(page.find('div', class_='description overflow'))
    hours = get_text(page.find('div', class_='section hours'))
    address = get_text(page.find('div', class_='section location'))
    phone = get_text(page.find('div', class_='detail_section phone'))

    if kind == 'resturant':
        data_dict['name'] = get_text(page.find('h1', class_='heading_title'))
    data_dict['comment'] = get_text(page.find('div', class_='prw_reviews_text_summary_hsx'))
    data_dict['popularity'] = popularity
    #data_dict['location'] = {'lat': float(lat), 'long': float(lng)}
    data_dict['images'] = images
    data_dict['url'] = url
    data_dict['description'] = description
    data_dict['hours'] = hours
    data_dict['address'] = address
    data_dict['phone'] = phone
    data_dict['type'] = kind
    data_dict['country'] = crawler_list['country']
    data_dict['state'] = crawler_list['state']
    printer('yellow', kind, '%s - %s' % (data_dict['country'], data_dict['state']))
    return data_dict


def get_poi_list(url, crawler_list, kind):
    pois = []
    page = get_page(url)
    #print(page)

    if kind == 'things-to-do':
        items = page.find_all('div', class_='attraction_clarity_cell')
    else:
        items = page.find_all('div', id=lambda x: x and x.startswith('eatery_'))

    for item in items:
        poi = {'href': item.find('a').get('href'),
               'name': item.find('a').get_text().strip()}
        url = base_url + poi['href']
        if is_exists(url):
            continue
        poi_data = ''
        if url.endswith('html.html'):
            printer('yellow', 'Not download', url)
            continue
        try:
            poi_data = get_poi(url, poi, crawler_list, kind)
        except Exception as e:
            msg = '%s - %s' % (url, e)
            printer('red', 'Error', msg)
        if poi_data:
            set_data(poi_data)
    return pois


def make_pages_and_normalize_input(loop, keys):
    if loop == 0:
        page = ''
    else:
        page = 'oa%s-' % (loop * 30)

    if 'state' in keys:
        state = keys['state']
    else:
        state = keys['country']

    printer('green', 'Country', keys['country'])

    if 'name' in keys:
        name = keys['name']
    else:
        name = keys['country']

    return page, state, name


def get_images(satl_obj):
    count = satl_obj.count_files()
    if count != 0:
        return False
    index = 1
    for url in satl_obj.get('images'):
        printer('cyan', 'Download', url)
        try:
            img = requests.get(url)
            satl_obj.attach_file_object(img.content, '%s.jpg' % index)
        except:
            pass
        index += 1
    return True


def crawl_things_to_do_city(keys):
    for x in xrange(0, 6):
        page, state, name = make_pages_and_normalize_input(x, keys)
        url = top_activity_url % (keys['index'], page, name)
        get_poi_list(url, keys, 'things-to-do')
    return True


def crawl_resturant_city(keys):
    for x in xrange(0, 1):
        page, state, name = make_pages_and_normalize_input(x, keys)
        url = top_restaurants_url % (keys['index'], page, keys['name'])
        get_poi_list(url, keys, 'resturant')
    return True


def get_detail_of_city(keys):
    #crawl_resturant_city(keys)
    crawl_things_to_do_city(keys)


def get_cities(keys):
    url = top_activity_url % (keys['index'], '', keys['country'])

    page = get_page(url)
    sidebar = page.find_all('div', class_='navigation_list')
    #print(page)
    elements = sidebar[1].find_all('div', class_='ap_navigator')
    items = []
    for element in elements:
        url_bones = element.find('a').get('href').split('-')
        if len(url_bones) == 4:
            name = url_bones[3].replace('.html', '')
        else:
            name = url_bones[4].replace('.html', '')
        items.append(
            {'index': url_bones[1], 'name': url_bones[3], 'state': get_text(element).replace('Things to do in ', ''),
             'country': keys['country']})

    return items


def is_exists(url):
    return Satl.is_exists(url)


def set_data(data):
    # if is_exists(data['url']):
    #     return False
    data['create_date'] = datetime.now()
    data['updated'] = False
    satl = Satl(data['url'],data=data,)
    printer('magenta', 'Save', " %s - %s" % (satl.pk, satl.get('name')))
    satl.save()
    get_images(satl)
    

    # this part writen beacuse of update images
    # else:
    #     satl = Satl(data['url']).load()
    return False

 
def main():
    countries = [
        # {'index': 'g293998', 'country': 'Iran'},
        # {'index': 'g293860', 'country': 'India'},
        # {'index': 'g294459', 'country': 'Russia'},
        # {'index': 'g294211', 'country': 'China'},
        # {'index': 'g187275', 'country': 'Germany'},
        # {'index': 'g187768', 'country': 'Italy'},
        # {'index': 'g293969', 'country': 'Turkey'},
        # {'index': 'g187427', 'country': 'Spain'}, 
        # {'index': 'g187070', 'country': 'France'},
        # {'index': 'g293951', 'country': 'Malaysia'},
        {'index': 'g293915', 'country': 'Thailand'},
    ]
    cities = []
    for country in countries:
        cities += get_cities(country)

    for item in cities:
        get_detail_of_city(item)
    return


if __name__ == "__main__":
    main()


